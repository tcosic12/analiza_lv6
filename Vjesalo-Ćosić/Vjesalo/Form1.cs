﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Vjesalo
{
    public partial class FormVjesalo : Form
    {
        string target;
        char guess;
        bool hit=false;
        int tries=9;
        public FormVjesalo()
        {
            InitializeComponent();
        }
        List<Rijeci> Wordlist = new List<Rijeci>();
        string path = "C:\\Users\\Tomislav\\Desktop\\drzave.txt";
        private void FormVjesalo_Load(object sender, EventArgs e)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(@path))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    string[] parts = line.Split('\t');
                    Rijeci R = new Rijeci(parts[0]);
                    Wordlist.Add(R);
                }
                listBox_word.DataSource = null;
                listBox_word.DataSource = Wordlist;

             }
            lbl_tries.Text = tries.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var random = new Random();

            int index = random.Next(0, listBox_word.Items.Count);
            target = $"{listBox_word.Items[index]}";
            Settings();
        }
        private void Settings()
        {
            lbl_word.Text = "";
            for(int i = 0; i < target.Length; i++)
            {
                lbl_word.Text = lbl_word.Text.Insert(i,"-");
            }
        }

        private void btn_guese_Click(object sender, EventArgs e)
        {
            guess = txtbox_guese.Text[0];
            for(int i = 0; i < target.Length; i++)
            {
                if (target[i] == guess)
                {
                    hit = true;
                    lbl_word.Text = lbl_word.Text.Remove(i,1);
                    lbl_word.Text = lbl_word.Text.Insert(i,guess.ToString());
                }
            }
            if (lbl_word.Text.ToUpper() == target.ToUpper())
                WonGame();
            if (!hit)
            {
                tries--;
                lbl_tries.Text = tries.ToString();
                if (tries == 0)
                {
                    LostGame();
                }
            }
            hit = false;
        }
        private void LostGame()
        {
            MessageBox.Show("You lost! Word was " + target);
            Resetgame();
        }

        private void WonGame()
        {
            MessageBox.Show("You Won! Word was "+ target);
            Resetgame();
        }
        private void Resetgame()
        {
            txtbox_guese.Text = "";
            lbl_word.Text = "";
            tries = 9;
            lbl_tries.Text = tries.ToString();
        }
    }
}
